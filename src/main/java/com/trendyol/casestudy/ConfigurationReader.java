package com.trendyol.casestudy;

import com.trendyol.casestudy.database.DataRepository;
import com.trendyol.casestudy.database.MongoDataRepository;
import com.trendyol.casestudy.model.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ConfigurationReader {

    // -----------------------------------------------------------------------------
    // Section: Instance variables
    // -----------------------------------------------------------------------------

    /**
     * Application name. ConfigurationReader can only retrieve records that belongs to this application name.
     */
    private final String applicationName;

    /**
     * Interface to access condiguration records.
     */
    private final DataRepository dataRepository;

    /**
     * A mapping that stores configuration instances of application by their name field.
     */
    /* Used map in order to: provide faster access in large cases, and it keeps old configurations*/
    private final Map<String, Configuration> configurationsByName;

    /**
     * A ScheduledExecutorService instance in order to update configuration records periodically.
     */
    private ScheduledExecutorService scheduledExecutorService;

    // -----------------------------------------------------------------------------
    // Section: Constructors
    // -----------------------------------------------------------------------------

    public ConfigurationReader(String applicationName, String connectionString, long refreshTimerIntervalInMs) {
        this.applicationName = Objects.requireNonNull(applicationName);
        this.dataRepository = new MongoDataRepository(connectionString);
        this.configurationsByName = new HashMap<>();

        // Manually update records first time
        updateConfigurations();
        // Schedule an update task
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(this::updateConfigurations, refreshTimerIntervalInMs, refreshTimerIntervalInMs, TimeUnit.MILLISECONDS);
    }

    // -----------------------------------------------------------------------------
    // Section: Methods
    // -----------------------------------------------------------------------------

    public <T> T getValue(String key) {
        Configuration configuration = configurationsByName.get(key);

        if (configuration != null && configuration.isActive() == 1) {
            try {
                return (T) configuration.getValue();
            } catch (ClassCastException e) {
                return null;
            }
        }
        return null;
    }

    private void updateConfigurations() {
        for (Configuration configuration : dataRepository.findConfigurationsByAppName(applicationName)) {
            configurationsByName.put(configuration.getName(), configuration);
        }
    }
}
