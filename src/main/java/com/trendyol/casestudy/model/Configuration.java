package com.trendyol.casestudy.model;


/**
 * This class represents Configuration objects that we store in MongoDB.
 *
 * @author Huseyin Celal Oner
 * @since 1.0
 */
public class Configuration {

    // -----------------------------------------------------------------------------
    // Section: Instance variables
    // -----------------------------------------------------------------------------

    private final int id;
    private final String name;
    private final String type;
    private final Object value;
    private final int isActive;
    private final String applicationName;

    // -----------------------------------------------------------------------------
    // Section: Constructors
    // -----------------------------------------------------------------------------

    public Configuration(int id, String name, String type, Object value, int isActive, String applicationName) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.value = value;
        this.isActive = isActive;
        this.applicationName = applicationName;
    }

    // -----------------------------------------------------------------------------
    // Section: Getters
    // -----------------------------------------------------------------------------

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    public int isActive() {
        return isActive;
    }

    public String getApplicationName() {
        return applicationName;
    }

    // -----------------------------------------------------------------------------
    // Section: Other methods
    // -----------------------------------------------------------------------------

    @Override
    public String toString() {
        return getClass().getName() + "[" +
                "id=" + getId() + ", " +
                "name=" + getName() + ", " +
                "type=" + getType() + ", " +
                "value=" + getValue() + ", " +
                "isActive=" + isActive() + ", " +
                "applicationName=" + getApplicationName() + ", " +
                "]";
    }

}

