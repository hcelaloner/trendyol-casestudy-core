package com.trendyol.casestudy.database;

import com.trendyol.casestudy.model.Configuration;

import java.util.List;

public interface DataRepository {
    List<Configuration> findConfigurationsByAppName(String applicationName);
}
