package com.trendyol.casestudy.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.trendyol.casestudy.model.Configuration;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class MongoDataRepository implements DataRepository {

    /**
     * A MongoClient instance
     */
    private MongoClient mongoClient;

    /**
     * Name of the database
     */
    private String mongoDatabaseName;

    /**
     * Name of the collection
     */
    private String mongoCollectionName;

    /**
     * Mongodb uri like 'mongo'
     */
    private String mongoConnectionString;

    public MongoDataRepository(String extendedConnectionString) {
        // Parse connection string first
        parseConnectionString(extendedConnectionString);
        // Then create mongo client
        mongoClient = createMongoClient(mongoConnectionString);
    }

    @Override
    public List<Configuration> findConfigurationsByAppName(String applicationName) {
        List<Configuration> configurations = new ArrayList<>();

        MongoCollection<Document> configurationsCollection = mongoClient.getDatabase(mongoDatabaseName)
                .getCollection(mongoCollectionName);

        configurationsCollection.find(Filters.eq("applicationName", applicationName))
                .map(document -> new Configuration(
                        document.getInteger("_id"),
                        document.getString("name"),
                        document.getString("type"),
                        document.get("value"),
                        document.getInteger("isActive"),
                        document.getString("applicationName")
                ))
                .into(configurations);

        return configurations;
    }

    /**
     * Sets mongodb related fields based on given connection string.
     *
     * @param connectionString a valid mongo URI, for instance mongodb://host:port/db/collection
     * @throws IllegalArgumentException if {@code connectionString} is null or empty
     */
    private void parseConnectionString(String connectionString) {
        if (connectionString == null || connectionString.isEmpty()) {
            throw new IllegalArgumentException("connectionString must not be null or empty.");
        }

        int lastSlashIndex = connectionString.lastIndexOf("/");
        // Parse collection name
        mongoCollectionName = connectionString.substring(lastSlashIndex + 1);
        // Parse database name
        mongoDatabaseName = connectionString.substring(connectionString.lastIndexOf("/", lastSlashIndex - 1) + 1, lastSlashIndex);
        // Parse connection string compatible with MongoClient
        mongoConnectionString = connectionString.substring(0, connectionString.lastIndexOf("/", lastSlashIndex - 1));
    }

    /**
     * Creates a Mongo instance based on given {@code connectionString}.
     *
     * @param connectionString a valid mongo URI
     * @return a Mongo instance which can be used to access databases in given {@code connectionString}.
     * @throws IllegalArgumentException if {@code connectionString} is null or empty
     */
    private MongoClient createMongoClient(String connectionString) {
        if (connectionString == null || connectionString.isEmpty()) {
            throw new IllegalArgumentException("connectionString must not be null or empty.");
        }
        // I didn't check connectionString extensively. Probably MongoClientURI class performs a validation.
        return new MongoClient(new MongoClientURI(connectionString));
    }

}
