package com.trendyol.casestudy.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConfigurationTest {

    private static final int EXPECTED_ID = 1;
    private static final String EXPECTED_NAME = "SiteName";
    private static final String EXPECTED_TYPE = "String";
    private static final String EXPECTED_VALUE = "trendyol.com";
    private static final int EXPECTED_IS_ACTIVE = 1;
    private static final String EXPECTED_APPLICATION_NAME = "SERVICE-A";

    private Configuration configuration;

    @Before
    public void initialize() {
        configuration = new Configuration(
                EXPECTED_ID,
                EXPECTED_NAME,
                EXPECTED_TYPE,
                EXPECTED_VALUE,
                EXPECTED_IS_ACTIVE,
                EXPECTED_APPLICATION_NAME
        );
    }

    @Test
    public void testId() {
        Assert.assertEquals(configuration.getId(), EXPECTED_ID);
    }

    @Test
    public void testName() {
        Assert.assertEquals(configuration.getName(), EXPECTED_NAME);
    }

    @Test
    public void testType() {
        Assert.assertEquals(configuration.getType(), EXPECTED_TYPE);
    }

    @Test
    public void testValue() {
        Assert.assertEquals(configuration.getValue(), EXPECTED_VALUE);
    }

    @Test
    public void testIsActive() {
        Assert.assertEquals(configuration.isActive(), EXPECTED_IS_ACTIVE);
    }

    @Test
    public void testApplicationName() {
        Assert.assertEquals(configuration.getApplicationName(), EXPECTED_APPLICATION_NAME);
    }

}
