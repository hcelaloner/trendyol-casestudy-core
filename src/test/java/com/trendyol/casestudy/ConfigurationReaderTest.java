package com.trendyol.casestudy;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ConfigurationReaderTest {

    @Before
    public void initialize() {
        MongoCollection<Document> configurationsCollection = new MongoClient(new MongoClientURI(TestConstants.MONGO_CONNECTION_STRING))
                .getDatabase(TestConstants.MONGO_DATABASE_NAME)
                .getCollection(TestConstants.MONGO_CONFIGURATIONS_COLLECTION_NAME);

        // Delete all sampleDocuments
        configurationsCollection.deleteMany(new Document());

        List<Document> sampleDocuments = Arrays.asList(
                new Document("_id", 1).append("name", "SiteName").append("type", "String").append("value", "trendyol.com").append("isActive", 1).append("applicationName", "SERVICE-A"),
                new Document("_id", 3).append("name", "MaxItemCount").append("type", "Int").append("value", 50).append("isActive", 0).append("applicationName", "SERVICE-A"),
                new Document("_id", 2).append("name", "IsBasketEnabled").append("type", "Boolean").append("value", 1).append("isActive", 1).append("applicationName", "SERVICE-B")
        );

        // Add sample sampleDocuments
        configurationsCollection.insertMany(sampleDocuments);
    }

    @Test
    public void testServiceA() {
        // Refresh - 0.5 min
        ConfigurationReader configurationReader = new ConfigurationReader("SERVICE-A", "mongodb://localhost:27017/test/configurations", 30000);

        // Test According to initial samples
        Assert.assertEquals(configurationReader.<String>getValue("SiteName"), "trendyol.com");
        Assert.assertEquals(configurationReader.<Integer>getValue("MaxItemCount"), null);
        Assert.assertEquals(configurationReader.<Boolean>getValue("IsBasketEnabled"), null);

        // Change some documents
        MongoCollection<Document> configurationsCollection = new MongoClient(new MongoClientURI(TestConstants.MONGO_CONNECTION_STRING))
                .getDatabase(TestConstants.MONGO_DATABASE_NAME)
                .getCollection(TestConstants.MONGO_CONFIGURATIONS_COLLECTION_NAME);

        configurationsCollection.replaceOne(
                new Document("_id", 1),
                new Document("_id", 1).append("name", "SiteName").append("type", "String").append("value", "trendyol.com").append("isActive", 0).append("applicationName", "SERVICE-A")
        );

        // Test According to initial samples
        Assert.assertEquals(configurationReader.<String>getValue("SiteName"), "trendyol.com");
        Assert.assertEquals(configurationReader.<Integer>getValue("MaxItemCount"), null);
        Assert.assertEquals(configurationReader.<Boolean>getValue("IsBasketEnabled"), null);

        // Sleep 0.5 min
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Test again
        Assert.assertEquals(configurationReader.<String>getValue("SiteName"), null);
        Assert.assertEquals(configurationReader.<Integer>getValue("MaxItemCount"), null);
        Assert.assertEquals(configurationReader.<Boolean>getValue("IsBasketEnabled"), null);

        // Change again
        configurationsCollection.replaceOne(
                new Document("_id", 1),
                new Document("_id", 1).append("name", "SiteName").append("type", "String").append("value", "trendyol2.com").append("isActive", 1).append("applicationName", "SERVICE-A")
        );

        // Test again
        Assert.assertEquals(configurationReader.<String>getValue("SiteName"), null);
        Assert.assertEquals(configurationReader.<Integer>getValue("MaxItemCount"), null);
        Assert.assertEquals(configurationReader.<Boolean>getValue("IsBasketEnabled"), null);

        // Sleep 0.5 min
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Test again
        Assert.assertEquals(configurationReader.<String>getValue("SiteName"), "trendyol2.com");
        Assert.assertEquals(configurationReader.<Integer>getValue("MaxItemCount"), null);
        Assert.assertEquals(configurationReader.<Boolean>getValue("IsBasketEnabled"), null);

    }
}
