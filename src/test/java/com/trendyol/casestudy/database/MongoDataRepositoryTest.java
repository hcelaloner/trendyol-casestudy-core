package com.trendyol.casestudy.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.trendyol.casestudy.TestConstants;
import com.trendyol.casestudy.model.Configuration;
import org.bson.Document;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MongoDataRepositoryTest {

    /**
     * Test findConfigurationsByAppName(String applicationName) method.
     */
    @Test
    public void testFindConfigurationsByAppName() {
        DataRepository mongoDataRepository = new MongoDataRepository(
                TestConstants.MONGO_CONNECTION_STRING + "/" + TestConstants.MONGO_DATABASE_NAME + "/" + TestConstants.MONGO_CONFIGURATIONS_COLLECTION_NAME
        );

        MongoCollection<Document> configurationsCollection = new MongoClient(new MongoClientURI(TestConstants.MONGO_CONNECTION_STRING))
                .getDatabase(TestConstants.MONGO_DATABASE_NAME)
                .getCollection(TestConstants.MONGO_CONFIGURATIONS_COLLECTION_NAME);

        // Delete all sampleDocuments
        configurationsCollection.deleteMany(new Document());

        // Case 1 - Test - empty database
        Assert.assertEquals(mongoDataRepository.findConfigurationsByAppName("SERVICE-A").size(), 0);
        Assert.assertEquals(mongoDataRepository.findConfigurationsByAppName("SERVICE-B").size(), 0);
        Assert.assertEquals(mongoDataRepository.findConfigurationsByAppName("SERVICE-C").size(), 0);

        List<Document> sampleDocuments = Arrays.asList(
                new Document("_id", 1).append("name", "SiteName").append("type", "String").append("value", "trendyol.com").append("isActive", 1).append("applicationName", "SERVICE-A"),
                new Document("_id", 3).append("name", "MaxItemCount").append("type", "Int").append("value", 50).append("isActive", 0).append("applicationName", "SERVICE-A"),
                new Document("_id", 2).append("name", "IsBasketEnabled").append("type", "Boolean").append("value", 1).append("isActive", 1).append("applicationName", "SERVICE-B")
        );

        // Add sample sampleDocuments
        configurationsCollection.insertMany(sampleDocuments);

        // Case 2 - Test - non empty database
        List<Configuration> serviceAConfigurations = mongoDataRepository.findConfigurationsByAppName("SERVICE-A");
        Assert.assertEquals(serviceAConfigurations.size(), 2);

        // Check first one
        Configuration configuration = serviceAConfigurations.get(0);
        Document expectedSampleDocument = sampleDocuments.get(0);
        Assert.assertEquals(configuration.getId(), expectedSampleDocument.getInteger("_id").intValue());
        Assert.assertEquals(configuration.getName(), expectedSampleDocument.getString("name"));
        Assert.assertEquals(configuration.getType(), expectedSampleDocument.getString("type"));
        Assert.assertEquals(configuration.getValue(), expectedSampleDocument.get("value"));
        Assert.assertEquals(configuration.isActive(), expectedSampleDocument.getInteger("isActive").intValue());
        Assert.assertEquals(configuration.getApplicationName(), expectedSampleDocument.getString("applicationName"));

        // Check second one
        configuration = serviceAConfigurations.get(1);
        expectedSampleDocument = sampleDocuments.get(1);
        Assert.assertEquals(configuration.getId(), expectedSampleDocument.getInteger("_id").intValue());
        Assert.assertEquals(configuration.getName(), expectedSampleDocument.getString("name"));
        Assert.assertEquals(configuration.getType(), expectedSampleDocument.getString("type"));
        Assert.assertEquals(configuration.getValue(), expectedSampleDocument.get("value"));
        Assert.assertEquals(configuration.isActive(), expectedSampleDocument.getInteger("isActive").intValue());
        Assert.assertEquals(configuration.getApplicationName(), expectedSampleDocument.getString("applicationName"));

        List<Configuration> serviceBConfigurations = mongoDataRepository.findConfigurationsByAppName("SERVICE-B");
        Assert.assertEquals(serviceBConfigurations.size(), 1);

        // Check first one
        configuration = serviceBConfigurations.get(0);
        expectedSampleDocument = sampleDocuments.get(2);
        Assert.assertEquals(configuration.getId(), expectedSampleDocument.getInteger("_id").intValue());
        Assert.assertEquals(configuration.getName(), expectedSampleDocument.getString("name"));
        Assert.assertEquals(configuration.getType(), expectedSampleDocument.getString("type"));
        Assert.assertEquals(configuration.getValue(), expectedSampleDocument.get("value"));
        Assert.assertEquals(configuration.isActive(), expectedSampleDocument.getInteger("isActive").intValue());
        Assert.assertEquals(configuration.getApplicationName(), expectedSampleDocument.getString("applicationName"));

        List<Configuration> serviceCConfigurations = mongoDataRepository.findConfigurationsByAppName("SERVICE-C");
        Assert.assertEquals(serviceCConfigurations.size(), 0);
    }


}
